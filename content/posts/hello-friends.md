---
title: "Hello Friends"
date: 2020-03-26T17:38:03-05:00
draft: false
---

That post title isn't a [Scott Hanselman](https://www.hanselman.com/) *ripoff*--it's an *homage*! (Don't worry Scott--I'm not stealing your tagline...)

My name is Shane. I'm a Software Developer who's been pulled into management, but I like to think that at my core I'm still a Developer.

I've been following Scott Hanselman's work since I saw him speak at a conference in the early 2000's. If you're just starting out in tech, you could do much worse than checking out his [youtube channel](https://www.youtube.com/channel/UCL-fHOdarou-CR2XUmK48Og), and specifically his series called [Computer Stuff They Didn't Teach You](https://www.youtube.com/watch?v=TtiBhktB4Qg&list=PL0M0zPgJ3HSesuPIObeUVQNbKqlw5U2Vr).

Anyway, I watched his most recent video where he was giving a [talk to students about Mentorship](https://www.youtube.com/watch?v=8HE5LJwAv1k), and he inspired me to get more [value out of my keystrokes](https://www.keysleft.com/#), and to "own my content". To that end, I've blown out this quick AWS S3 website using Hugo as the static site generator. I'm no stranger to blogging--I've had a couple of false starts. Once was when I tried to [teach myself to play the mandolin](http://teacherandthefool.blogspot.com/), and another was a one-post attempt at a [technical blog](http://blog.shane-gibbons.com/). Neither the music endeavor nor the blog lasted very long unfortunately, but Scott has once again inspired me, so let's see what happens. I've spent the last couple of evenings mucking around with AWS, S3, Cloudfront and Hugo, so I'll keep this introductory post short and sweet. More to come.

Thanks Scott.

# My Blog

Static web site using the [hugo](https://gohugo.io/) generator and hosted on AWS S3 via Cloudfront for that sweet, sweet https protocol.

The blog is hosted at [blog.shanegibbons.com](https://blog.shanegibbons.com).

build:
	hugo

deploy: build
	aws s3 cp public s3://blog.shanegibbons.com/ --recursive
